import numpy as np
from numba import njit, prange
import time

def range_test(A):
    s = 0
    for i in range(A.shape[0]):
        s += A[i]
    return s

@njit(parallel=True) # Setting parallel=True attempts to automatically parallelize a function
def prange_test(A):
    s = 0
    # prange specifies that a loop can be parallelized
    for i in prange(A.shape[0]): # Without "parallel=True" in the jit-decorator, the prange statement is equivalent to range
        s += A[i]
    return s

my_lst = np.random.random(10000000)

start_1 = time.time()
range_test(my_lst)
end_1 = time.time()
print("Seconds elapsed (with compilation) = %s" % (end_1 - start_1))

start_1 = time.time()
range_test(my_lst)
end_1 = time.time()
print("Seconds elapsed (after compilation) = %s" % (end_1 - start_1))

start_2 = time.time()
prange_test(my_lst)
end_2 = time.time()
print("Seconds elapsed utilizing Numba (with compilation) = %s" % (end_2 - start_2))

start_2 = time.time()
prange_test(my_lst)
end_2 = time.time()
print("Seconds elapsed utilizing Numba (after compilation) = %s" % (end_2 - start_2))
