#!/bin/bash

#SBATCH --job-name=numba_sum2d    ## Job name
#SBATCH --output=numba_sum2d.out  ## Output file
#SBATCH --time=10:00            ## Limit of wall clock time
#SBATCH --nodes=1-5             ## Number of different nodes
#SBATCH --ntasks=1              ## Number of tasks
#SBATCH --cpus-per-task=1       ## Number of CPUs (or cores) per task
#SBATCH --mem-per-cpu=2G        ## Memory required per allocated CPU

## Load python interpreter
module load Conda/3

## Execute numba_sum2d script
srun python numba_sum2d.py
