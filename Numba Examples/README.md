### Software Abstract
The software I chose to evaluate in my project is Python Numba. It is a compiler aiming to speed up applications. Numba is designed for Python arrays and numerical functions; specifically, it works best on code that uses loops, Numpy arrays and functions. Without having to switch languages, it increases optimization for code written in Python that is particularly math-heavy, utilizing functions with high performance. In order to call on Numba to compile functions, a selection of decorators are available to be applied. Numba’s main feature is the jit decorator, which marks a function for optimization, and Numba decides how to optimize. The option to parallelize functions automatically is available in the Numba decorators as well.
### Installation
In order to install Python Numba on the HPCC, here are provided step-by-step instructions:
1. Log onto the HPCC
2. Log onto a development node
3. Run this command: `pip install numba`
4. To check if installation was successful, run these commands:
```
python
import numba
numba.__version__
```
This should output the version of Numba you have installed.

If pip installation is unsuccessful for you, follow these instructions:
1. Log onto the HPCC
2. Log onto a development node
3. If you do not have Anaconda installed on the HPCC, follow the steps provided here: https://docs.icer.msu.edu/Using_conda/. To check if you already have it installed, type the command `conda --version` and it will output the version you have.
4. Once you have properly installed Anaconda, load the module by running this command: `module load Conda/3`.
5. Run this command: `conda install numba`
6. To check if installation was successful, run these commands:
```
python
import numba
numba.__version__
```
This should output the version of Numba you have installed.

For further instruction on installation, refer to Numba’s user manual: https://numba.readthedocs.io/en/stable/user/installing.html
### Example Code
In order to run the example code within this directory on the HPCC, here are provided step-by-step instructions:
1. Log onto the HPCC
2. Log onto a development node
3. Clone this repository in the directory desired
4. Navigate to this directory: `cd 'Numba Examples'`
5. Type this command to see the example code: `cat numba_std.py`
6. Type this command to run the example code: `python numba_std.py`
### References
https://numba.readthedocs.io/en/stable/index.html

https://thedatafrog.com/en/articles/make-python-fast-numba/
