import math
import numpy as np
import numba
from numba import njit
import time

def std(lst):
    mean = 0
    for i in lst:
        mean += i
    mean /= len(lst)
    v = 0
    for i in lst:
        v += (i - mean)**2
    variance = v/len(lst)
    std = math.sqrt(variance)
    return std

fast_std = njit(std)

my_lst = np.random.normal(0, 1, 10000000)

start_1 = time.time()
std(my_lst)
end_1 = time.time()
print("Seconds elapsed (with compilation) = %s" % (end_1 - start_1))

start_1 = time.time()
std(my_lst)
end_1 = time.time()
print("Seconds elapsed (after compilation) = %s" % (end_1 - start_1))

start_2 = time.time()
fast_std(my_lst)
end_2 = time.time()
print("Seconds elapsed utilizing Numba (with compilation) = %s" % (end_2 - start_2))

start_2 = time.time()
fast_std(my_lst)
end_2 = time.time()
print("Seconds elapsed utilizing Numba (after compilation) = %s" % (end_2 - start_2))
