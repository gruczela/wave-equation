import numpy as np
from numba import jit
import time

def sum2d(arr):
    M, N = arr.shape
    result = 0.0
    for i in range(M):
        for j in range(N):
            result += arr[i,j]
    return result

@jit
def fast_sum2d(arr):
    M, N = arr.shape
    result = 0.0
    for i in range(M):
        for j in range(N):
            result += arr[i,j]
    return result

arr = np.array([[1,2,3],[4,5,6]])


start_1 = time.time()
sum2d(arr)
end_1 = time.time()
print("Seconds elapsed (with compilation) = %s" % (end_1 - start_1))

start_1 = time.time()
sum2d(arr)
end_1 = time.time()
print("Seconds elapsed (after compilation) = %s" % (end_1 - start_1))

start_2 = time.time()
fast_sum2d(arr)
end_2 = time.time()
print("Seconds elapsed utilizing Numba (with compilation) = %s" % (end_2 - start_2))

start_2 = time.time()
fast_sum2d(arr)
end_2 = time.time()
print("Seconds elapsed utilizing Numba (after compilation) = %s" % (end_2 - start_2))
