#!/bin/bash

#SBATCH --job-name=numba_std    ## Job name
#SBATCH --output=numba_std.out  ## Output file
#SBATCH --time=10:00            ## Limit of wall clock time
#SBATCH --nodes=1-5             ## Number of different nodes
#SBATCH --ntasks=1              ## Number of tasks
#SBATCH --cpus-per-task=1       ## Number of CPUs (or cores) per task
#SBATCH --mem-per-cpu=2G        ## Memory required per allocated CPU

## Load python interpreter
module load Conda/3

## Execute numba_std script
srun python numba_std.py

scontrol show job $SLURM_JOB_ID  ## Write job information to SLURM output file
